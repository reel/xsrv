# Change Log

All notable changes to this project will be documented in this file.  
The format is based on [Keep a Changelog](http://keepachangelog.com/).


-------------------------------


#### [v1.0](https://gitlab.com/nodiscc/xsrv/-/releases#1.0) - UNRELEASED

This is a major rewrite of https://github.com/nodiscc/srv01. It improves usability, portability, standards compliance, separation of concerns, performance, documentation, security, simplifies installation and usage, and adds many new features to all roles/components. There is no easy upgrade path from previous releases, you must redeploy to a new instance, and restore backups/exports of all user data.

A summary of changes is included below. See [README.md](README.md) for more information.

**xsrv command-line tool**
- improve/simplify command-line usage, see `xsrv help`
- refactor main script/simplify/cleanup
- make installation to $PATH optional


**example playbook: refactor:**
- add examples for playbook, inventory, and host_vars (cleartext and vaulted) files
- disable all role by default except common, backup, monitoring. Users should manually enable additional roles
- firewall: by default, allow incoming traffic for netdata dashboard from LAN (monitoring role is enabled by default)
- firewall: by default, allow incoming SSH from anywhere (key-based authentication is enabled so this is reasonably secure)
- firewall: by default, allow HTTP/HTTPS access from anywhere (required for let's encrypt http-01 challenge, and apache role is enabled by default)
- firewall: change the default policy for the 'global' firehol_network definition to RETURN (changes nothing in the default configuration, makes adding other network definitions easier)
- doc: add firewall examples for all services (only from LAN by default)
- doc: add example .gitlab-ci.yml
- ansible: use .ansible-vault-password as vault password file
- host_vars: add a netdata check for successful daily backups
- host_vars: add netdata process checks for ssh, fail2ban, ntp, httpd, sql
- host_vars: auto-restart services by default when needrestart detects a restart is required
- remove unused directories, cleanup


**common: refactor role:**
- import from https://gitlab.com/nodiscc/ansible-xsrv-common
- use ansible-vault to store secret values by default
- unattended-upgrades: allow automatic upgrades from stable-updates repository
- unattended-upgrades: install apt-listchanges (mail with a summary of changes will be sent to the server admin)
- add ansible_user_allow_sudo_rsync_nopasswd option (allow ansible user to run sudo rsync without password)
- msmtp: require manual configuration of msmtp host/username/password (if msmtp installation is enabled)
- dns: add ability to configure multiple DNS nameservers in /etc/resolv.conf
- packages: enable haveged installation by default
- packages: don't install pwgen/secure-delete/autojump by default, add man package
- sshd: remove deprecated UsePrivilegeSeparation option
- sshd: make ssh server log level, PasswordAuthentication, AllowTcpForwarding and PermitRootLogin options configurable
- sshd: fix accepted environment variables LANG,LC_* accepted from the client
- sshd: explicitely deny AllowTcpForwarding, AllowAgentForwarding, GatewayPorts and X11Forwarding for the sftponly group
- sshd: add curve25519-sha256@libssh.org KexAlgorithm
- firewall: allow outgoing mail submission/port 587 by default
- firewall: make firewall config file only readable by root
- firewall: use an alias/variable to define LAN IP networks, templatize
- firewall/fail2ban: prevent firehol from overwriting fail2Ban rules, remove interaction/integration between services
- firewall/fail2Ban: split firewall and fail2ban configuration tasks, add ability to disable both
- fail2ban: make more settings configurable (destination e-mail, default findtime/maxretry/bantime)
- users: simplify management, remove *remotebackup* options/special remotebackup user/tasks
- users: linux_users is now compatible with ansible users module syntax, with added ssh_authorized_keys and sudo_nopasswd_commands parameters
- users: fix user password generation (use random salt, make task idempotent by setting update_password: on_create by default)
- update documentation and role metadata
- use new syntax for ansible facts/distribution release
- various fixes, cleanup, formatting


**monitoring: refactor role:**
- import from https://gitlab.com/nodiscc/ansible-xsrv-monitoring
- netdata: add ssl/x509 expiration checks, make http check timeout value optional, default to 1s)
- netdata: allow installation from deb packages/packagecloud APT repository, make it the default
- netdata: decrease frequency of apache status checks to 10 seconds (decrease log spam)
- netdata: disable access logs and debug logs by default (performance), add netdata_disable_*_log variables to configure it
- netdata: disable cloud/SaaS features by default, add netdata_cloud_enabled variable to configure it
- netdata: disable web server gzip compression since we only use ssl
- netdata: install and configure https://gitlab.com/nodiscc/netdata-logcountmodule, disable notifications by default
- netdata: install and configure https://gitlab.com/nodiscc/netdata-modtime module
- netdata: make dbengine disk space size and memory page cache size configurable
- netdata: monitor mysql server if mariadb role is enabled (add netdata mysql user)
- netdata: upgrade to 1.24.0 when installed from binary
- rsyslog: aggregate all log messages to `/var/log/syslog` by default
- rsyslog: monitor samba and gitea log files with imfile module
- rsyslog: fix syslog tags for imfile watches
- rsyslog: make agregation of apache access logs to syslog optional, disable by default
- rsyslog: disable aggregation of netdata logs to syslog by default (very noisy, many false-positive ERROR messages)
- needrestart: don't auto-restart services by default
- various fixes, reorder, cleanup, update documentation, fix role/certificate generation idempotence


**backup role**
- import from https://gitlab.com/nodiscc/ansible-xsrv-backup
- auto-load rsnapshot configuration from /etc/rsnapshot.d/*.conf, remove hardcoded xsrv* roles integration
- check rsnapshot configuration after copying files
- restrict access to backups directory to root only
- write rsnapshot last success time to file (allows monitoring the time since last successful backup)
- store ssh public key to ansible facts (this will allow generating a human readable document/dashboard with hosts information)

**lamp role: refactor:**
- import from https://gitlab.com/nodiscc/ansible-xsrv-lamp
- split lamp role to separate apache and mariadb roles


**apache: refactor role:**
- use apache mod-md for Let's Encrypt certificate generation, remove certbot and associated ansible tasks
- rename cert_mode variable to https_mode
- don't enable mod-deflate by default
- remove SSLEngine directive from common SSL configuration (SSLEngine must be at the same level as the SSLCertificateFile directive)
- remove automatic homepage generation feature (will be split to separate role)
- always install libapache2-mod-md/apache2 from buster-backports, newer version with ACMEv2 support required
- the role depends on common role for apt backports sources installation/update apt cache handler
- update doc, cleanup, formatting, add screenshot
- use ansible-vault to manage secret variables
- do not setup any virtualhost by default
- set the default log format to `vhost_combined` (all vhosts to a single file)
- require manual configuration of the letsencrypt admin email address
- move common mod_md settings to apache role
- enforce root:www-data ownership and mode 0750 on virtualhosts documentroots
- disable X-Frame-Options header as Content-Security-Policy frame-ancestors replaces/obsoletes it
- mark HTTP->HTTPS redirects as permanent (HTTP code 301)
- disable setting a default Content-Security-Policy, each application is responsible for setting an appropriate CSP
- exclude /server-status from automatic HTTP -> HTTPS redirects
- ensure the default/fallback vhost is always the first in load order
- enforce fail2ban bans on HTTP basic auth failures
- allow defining custom reverse proxies in configuration


**nextcloud: refactor role:**
- import from https://gitlab.com/nodiscc/ansible-xsrv-nextcloud
- determine appropriate setup procedure depending on whether nextcloud is already installed or not, installed version and current role version (installation/upgrades are now idempotent)
- add support for let's encrypt certificates (use mod_md when nextcloud_rss_https_mode: letsencrypt. else generate self-signed certificates)
- use ansible local fact file to store nextcloud installed version
- ensure correct/restrictive permissions are set
- support postgresql as database engine, make it the default
- move apache configuration steps to separate file, add full automatic virtualhost configuration for nextcloud
- reorder setup procedure (setup apache last)
- use ansible-vault to manage secret variables by default
- enable additional php modules https://docs.nextcloud.com/server/16/admin_manual/installation/source_installation.html#apache-web-server-configuration
- reload apache instead of restarting when possible
- update documentation, add screenshots
- templatize nextcloud domain name/install directory/full URL
- require manual configuration of nextcloud FQDN
- enforce fail2ban bans on nextcloud login failures
- upgrade nextcloud to 19.0.1, upgrade all nextcloud apps
- add fine-grained ansible tags
- automatically install applications using occ app:install command, remove app-related variables and ansible tasks
- enable APCu memcache https://docs.nextcloud.com/server/19/admin_manual/configuration_server/caching_configuration.html
- gallery app replaced with photos app
- remove old installation directory at the end of upgrades
- make backup role fully optional, check rsnapshot configuration after copying config file
- delegate database backups to the respective database role (mariadb/postgresql)
- add deck, notes and maps apps


**tt-rss: refactor role:**
- import from https://gitlab.com/nodiscc/ansible-xsrv-tt-rss
- add support for postgresql databases, make it the default (config variable tt_rss_db_type)
- add support for postgresql backups/dumps
- make backup role fully optional, check rsnapshot configuration after copying config file
- delegate database backups to the respective database role (mariadb/postgresql)
- add support for let's encrypt certificates (use mod_md when tt_rss_https_mode: letsencrypt)
- make log destination configurable, default to blank/PHP/webserver logs
- update config.php template (remove deprecated feed_crypt_key setting)
- use ansible-vault to manage secret variables by default
- require manual configuration of admin username and tt-rss FQDN/URL
- standardize component installation order (backups/fail2ban/database first)
- simplify ansible_local.tt_rss.db_imported, always set to true
- do not use a temporary file to store admin user credentials, run mysql command directly from tasks
- add support for letsencrypt certificates/virtualhost configuration (mod_md). add tt_rss_https_mode: selfsigned/letsencrypt, move tasks to separate files
- mark plugins/themes setup tasks as unmaintained, move to separate yml files
- simplify file permissions setup/make idempotent
- update documentation
- simplify domain name/install directory/full URL templating
- rename role to `tt_rss`
- various fixes, cleanup, reordering

**Migrating tt-rss data to Postgresql from a MySQL-based installation:**

```bash
# OPML import/export (including filters and some settings). Must be done before data_migration plugin if you want to keep feed categories
# on the original machine
sudo mkdir /var/www/tt-rss/export
sudo chown -R www-data:www-data /var/www/tt-rss/export/
sudo -u www-data php /var/www/tt-rss/update.php --opml-export "MYUSERNAME /var/www/tt-rss/export/export-2020-08-07.opml" # export feeds OPML
# on a client
rsync -avP my.original.machine.org:/var/www/tt-rss/export/export-2020-08-07.opml ./ # download opml export
# login to the new tt-rss instance from a browser, go to Preferences > Feeds, import OPML file

# migrate all articles from mysql to postgresql
# on the original machine
git clone https://git.tt-rss.org/fox/ttrss-data-migration
sudo chown -R root:www-data ttrss-data-migration/
sudo mv ttrss-data-migration/ /var/www/tt-rss/plugins.local/data_migration
sudo nano /var/www/tt-rss/config.php # enable data_migration in the PLUGINS array
sudo -u www-data php /var/www/tt-rss/update.php --data_user MYUSERNAME --data_export /var/www/tt-rss/export/export-2020-08-07.zip # export articles to database-agnostic format

# on the target machine
git clone https://git.tt-rss.org/fox/ttrss-data-migration
sudo chown -R root:www-data ttrss-data-migration/
sudo mv ttrss-data-migration/ /var/www/rss.example.org/plugins.local/data_migration
sudo nano /var/www/rss.example.org/config.php # enable data_migration in the PLUGINS array
rsync -avP my.original.machine.org:/var/www/tt-rss/export/export-2020-08-07.zip ./
sudo mkdir /var/www/rss.example.org/export
sudo mv export-2020-08-07.zip /var/www/rss.example.org/export
sudo chown -R root:www-data /var/www/rss.example.org/export
sudo chmod -R g+rX /var/www/rss.example.org/export/
sudo -u www-data php /var/www/rss.example.org/update.php --data_user MYUSERNAME --data_import /var/www/rss.example.org/export/export-2020-08-07.zip # it can take a while
sudo rm -r /var/www/rss.example.org/export/ # cleanup

```

**gitea:**
- add gitea self-hosted software forge role (https://gitea.io/en-us/)
- import from https://gitlab.com/nodiscc/ansible-xsrv-gitea
- make backup role fully optional, check rsnapshot configuration after copying config file
- delegate database backups to the respective database role (mariadb/postgresql)
- make common settings configurable through ansible variables
- use ansible-vault to handle secret variables, dont generate random passwords
- simplify domain name/location/root URL templating
- require manual configuration of gitea instance FQDN/URL, JWT secrets and internal token
- default admin e-mail address to admin@{{ gitea_fqdn }}
- add checks for mandatory variables
- LFS JWT secret must not contain /+= characters
- only configure a subset of gitea settings in the configuration file, let gitea use defaut values for other settings
- disable displaying gitea version in footer
- update gitea to 1.12.3
- download binary from github.com instea of gitea.io
- download uncompressed binary to avoid handling xz decompression
- update configuration file template
- add support for self-signed and let's encrypt certificates through gitea_https_mode variable
- update documentation


**shaarli: refactor role:**
- detect installed version from ansible fact file and appropriate install/upgrade procedure depending on installed version
- add apache configuration for shaarli, including Content-Security Policy and SSL/TLS certificate management with mod_md
- allow generation of sel-signed certificates
- make shaarli fqdn and installation directory configurable
- harden file permissions
- add rsnapshot configuration for shaarli backups
- auto-configure shaarli from ansible variables (name/user/password/timezone) + compute hash during installation
- by default, don't overwrite shaarli config when it already exists (rely on configuration from the web interface) (idempotence)
- require manual generation of shaarli password salt (40 character string)
- add documentation, add backup restoration procedure
- use ansible-vault as default source for shaarli username, password, api secret and salt
- add role to example playbook (disabled by default)


**transmission: refactor role:
- install and configure transmission (most settings are left to their defaults)
- add ansible variables for username/password, service status, download directory, FQDN
- only let transmission web interface listen on 127.0.0.1
- settings.json is updates by the daemon on exit with current/user-defined settings, hence the role is not always idempotent
- configure an apache virtualhost for transmission
- add transmission_https_mode variable to configure SSL certificate generation (selfsigned/letsencrypt)
- add checks for required variables
- delegate HTTP basic auth to apache, pass credentials to the backend tranmission service (proxy-chain-auth)
- update documentation and role metadata
- add rsnapshot configuration (backup transmission downloads and torrents cache)


**postgresql role:**
- add basic postgresql role
- add optional integration with the backup role (automatic database backups)


**samba role:**
 - add samba file sharing server role (standalone mode)
 - make log level configurable
 - make shares configurable through samba_shares variable/list of dicts
 - make users configurable through samba_users variable/list of dicts
 - add rsnapshot backup configuration for samba
 - make shares available/browseable state configurable (default to yes)
 - update documentation

**tools, documentation, misc:**
- refactor and update documentation (clarify/cleanup/update/reorder/reword/simplify/deduplicate/move)
- import all roles from separate repositories to single repository
- publish roles and default playbook as ansible collection (https://galaxy.ansible.com/nodiscc/xsrv)
- fix automated tests (ansible-lint, yamllint, shellcheck), add ansible-playbook --syntax-check test
- remove pip install requirements (performed by xsrv script)
- move to release/dev/tag branching/release model
- generate TODO.md file with 'make update_todo'
- fully working ansible-playbook --check mode
- explicitely define file permissions for all file copy/templating tasks
- add checks/assertions for all mandatory variables
- remove .gitignore, clean files generated by tests using 'make clean'
- improve logging
